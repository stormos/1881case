﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Http.Headers;
using System.Text;
using System.Net.Http;
using System.Web;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using _1881Case_Storm.Models;

namespace _1881Case_Storm.Controllers
{
    [Route("/request")]
    [ApiController]
    public class RequestController : ControllerBase
    {

        /// <summary>
        /// Makes a general search for the specified string
        /// </summary>
        /// <param name="query">Search string</param>
        /// <param name="page">Page number</param>
        /// <param name="limit">Size of the page</param>
        [HttpGet("/search/{query}/{page:int?}/{limit:int?}")]
        public async Task<IActionResult> Search(string query, int page = 1, int limit = 100)
        {
            var unit = "unit";
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "4e389652343d4dc0b5cb0b4946f96115");
            client.DefaultRequestHeaders.Add("query", query);

            // Request parameters
            queryString["query"] = query;
            queryString["page"] = $"{page}";
            queryString["limit"] = $"{limit}";
            // Brukte en del tid på å sende inn riktig parameter grunnet blindhet i egne kode
            var uri = $"https://services.api1881.no/search/{unit}?" + queryString;

            var response = await client.GetAsync(uri);
            
            response.EnsureSuccessStatusCode();

            var people =await ConvertObjectsAndGetMoreData(await response.Content.ReadAsStringAsync());
            return Ok(people);
        }
        /// <summary>
        /// Searches for the number of a person
        /// </summary>
        /// <param name="query">The number to search for </param>
        /// <returns></returns>
        [HttpGet("/phone/{query}")]
        public async Task<IActionResult> PhoneSearch(string query)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(query);
            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "4e389652343d4dc0b5cb0b4946f96115");
            var uri = "https://services.api1881.no/lookup/phonenumber/" + queryString;
            var response = await client.GetAsync(uri);
            var people =await ConvertObjectsAndGetMoreData(await response.Content.ReadAsStringAsync());
            return Ok(people);
        }

        public async Task<List<object>> ConvertObjectsAndGetMoreData(string uriResp)
        {
            // Client burde gjøres til en delt resurs eller lagres utenfor funkjsonene men dette var noe jeg ikke fikk tid til å gjøre
            var returnList = new List<object>();
            //Hadde en del problemer med riktig konvertering fra Json til objecter
            var obj = JsonConvert.DeserializeObject<Response>(uriResp);
            foreach(var contract in obj.Contracts)
            {
                var _client = new HttpClient();
                _client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "4e389652343d4dc0b5cb0b4946f96115");
                var uri = "https://services.api1881.no/lookup/id/" + contract.Id;
                var response = await _client.GetAsync(uri);
                var respCont = await response.Content.ReadAsStringAsync();
                switch (contract.Type){
                    case "Person":
                        var person = JsonConvert.DeserializeObject<PersonResponse>(respCont).person[0];
                        
                        returnList.Add(person);
                        break;
                    case "Company":
                        var company = JsonConvert.DeserializeObject<OrganizationResponse>(respCont).Organizations[0];
                        company.Type = contract.Type;
                        company.OrganizationNumber = contract.OrganizationNumber;
                        company.Id = contract.Id;
                        company.PostArea = contract.PostArea;
                        company.PostCode = contract.PostCode;
                        returnList.Add(company);
                        break;
                    default:
                        throw new ArgumentException($"Unknown type:{contract.Type}");
                }
            }
            return returnList;
        }
    }
}
