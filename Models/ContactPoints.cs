﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _1881Case_Storm.Models
{
    public class ContactPoints
    {
        [JsonProperty(PropertyName = "label")]
        public string Label { get; set; }
        [JsonProperty(PropertyName = "main")]
        public bool? Main { get; set; }
        [JsonProperty(PropertyName = "contractType")]
        public string ContractType { get; set; }
        [JsonProperty(PropertyName ="type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName ="value")]
        public string Value { get; set; }
    }
    /*
    public enum ContactType
    {
        Phone,
        Email,
        WebAdress
    }
    */
}
