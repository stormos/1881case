﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _1881Case_Storm.Models
{
    public class Geography
    {
        [JsonProperty(PropertyName = "municipality")]
        public string Municipality { get; set; }
        [JsonProperty(PropertyName = "county")]
        public string Country { get; set; }
        [JsonProperty(PropertyName = "region")]
        public string Region { get; set; }
        [JsonProperty(PropertyName = "coordinate")]
        public Coordinate Coordinate { get; set; }
        [JsonProperty(PropertyName = "address")]
        public Address Address { get; set; }
    }
    public class Coordinate
    {
        [JsonProperty(PropertyName = "latitude")]
        public string Latitude { get; set; }
        [JsonProperty(PropertyName = "longitude")]
        public string Longitude { get; set; }
    }
    public class Address
    {

        [JsonProperty(PropertyName = "street")]
        public string Street { get; set; }

        [JsonProperty(PropertyName = "houseNumber")]
        public int? HouseNumber { get; set; }

        [JsonProperty(PropertyName = "entrance")]
        public string Entrance { get; set; }

        [JsonProperty(PropertyName = "postCode")]
        public int? PostCode { get; set; }

        [JsonProperty(PropertyName = "postArea")]
        public string PostArea { get; set; }

        [JsonProperty(PropertyName = "addressString")]
        public string AddressString { get; set; }
    }
}
