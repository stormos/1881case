﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace _1881Case_Storm.Models
{
    public class OrganizationResponse
    {
        [JsonProperty(PropertyName = "count")]
        public int Count { get; set; }
        [JsonProperty(PropertyName = "contacts")]
        public Organization[] Organizations { get; set; }
    }
    public class Organization
    {
        [JsonProperty(PropertyName = "organizationNumber")]
        public string OrganizationNumber { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "postCode")]
        public string PostCode { get; set; }
        [JsonProperty(PropertyName = "postArea")]
        public string PostArea { get; set; }
        [JsonProperty(PropertyName = "infoUrl")]
        public string InfoUrl { get; set; }
        [JsonProperty(PropertyName = "geography")]
        public Geography Geographies { get; set; }
        [JsonProperty(PropertyName = "contactPoints")]
        public ContactPoints[] ContactPoints { get; set; }
    }
}
