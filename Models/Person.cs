﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _1881Case_Storm.Models
{
    public class PersonResponse
    {
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }
        [JsonProperty(PropertyName = "contacts")]
        public Person[] person { get; set; }
    }
    public class Person
    {
        [JsonProperty(PropertyName ="firstName")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "geography")]
        public Geography Geography{ get; set; }
    }
}
