﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace _1881Case_Storm.Models
{
    public class Response
    {
        [JsonProperty(PropertyName ="count")]
        public int? Count { get; set; }
        [JsonProperty(PropertyName = "contacts")]
        public List<Organization> Contracts { get; set; }
    }
}
